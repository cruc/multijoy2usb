#include <stdlib.h> // sprintf

// Setting up the cable to board mapping, as this is still a prototype and will likely change.
//

// Which Lines are going to be used by the interface connected? Set this to 0xff to get all 4 lines back via USB,
// but debugging it is easier to mask out those not connected because they might wobble.
//const int activeLineMask = (1 << 4) | (1 << 5) | (1 << 6); // Use lines 4, 5, and 6 - new type CMOS interface with 3 multiplexers
const int activeLineMask = (1 << 5) | (1 << 7); // Use lines 5 and 7 - PE and Busy, old typ of TTL interface
//const int activeLineMask = 0xff;

// These are the Arduino out pins, on which we will write the multiplexer address.
// On my cable, I protected these against an accidential short damaging the microprocessor
// by adding in line a 470 ohm resistor on each of them.
const int ARDUINO_PIN_DATA0 =  2;
const int ARDUINO_PIN_DATA1 =  3;
const int ARDUINO_PIN_DATA2 =  4;
const int ARDUINO_PIN_DATA3 =  5;

// And these are the Arduino pins for the 4 possible input signals.
// Those are protected by a 230 ohm resistor in my cable.
// Note that we do not need all four lines for any of the interfaces, but the old ones
// used 2 of these, and the newer ones used 3, not including the originally used busy.
// Can't remember why.
const int ARDUINO_PIN_PAPER_EMPTY   =  6;
const int ARDUINO_PIN_BUSY          =  7;
const int ARDUINO_PIN_ACKNOWLEDGE   =  8;
const int ARDUINO_PIN_SELECT_ONLINE =  9;

// As we solve every problem by indirection, now map the ARDUINO input pins to the 
// original Pascal "Lines" we use in the 2.0 version of the config files.
// This is the old Pascal "Line" definition for the signals. 
// Port 889 doesn't use the lowest 3 bits, only the upper 5.
//
// Line 4 - Select/Online
// Line 5 - Paper Empty 
// Line 6 - Acknowledge
// Line 7 - Busy
//
// The old, 2-multiplexer interfaces use Paper Empty and Busy
const int PIN_LINE_4 = ARDUINO_PIN_SELECT_ONLINE;
const int PIN_LINE_5 = ARDUINO_PIN_PAPER_EMPTY;
const int PIN_LINE_6 = ARDUINO_PIN_ACKNOWLEDGE;
const int PIN_LINE_7 = ARDUINO_PIN_BUSY;


byte currentAddress = 0; // This is the looping variable that addresses the multiplexers

byte allPins[16]; // Store for each of the 16 multiplexer addresses the read-out of the bits
byte previousPins[16]; // To send only on change, store previous values transmitted

char numberOut[3]; // Static buffer for printf

void writeAddressToMultiplexer(int address) {
  digitalWrite(ARDUINO_PIN_DATA0, (address & 0x01) ? HIGH : LOW);
  digitalWrite(ARDUINO_PIN_DATA1, (address & 0x02) ? HIGH : LOW);
  digitalWrite(ARDUINO_PIN_DATA2, (address & 0x04) ? HIGH : LOW);
  digitalWrite(ARDUINO_PIN_DATA3, (address & 0x08) ? HIGH : LOW);
}

void setup() {
  // Initialize data structure
  for (int i = 0 ; i < 16; i++) {
    allPins[i] = 0;
    previousPins[i] = 0xff; // To make sure we have a first line
  }

  // Start talking
  Serial.begin(38400);

  // 4 bits are used as multiplexer address, set them as outputs
  pinMode(ARDUINO_PIN_DATA0, OUTPUT);
  pinMode(ARDUINO_PIN_DATA1, OUTPUT);
  pinMode(ARDUINO_PIN_DATA2, OUTPUT);
  pinMode(ARDUINO_PIN_DATA3, OUTPUT);

  // Set the inputs - for now, set all possible 4 inputs
  pinMode(PIN_LINE_4, INPUT);
  pinMode(PIN_LINE_5, INPUT);
  pinMode(PIN_LINE_6, INPUT);
  pinMode(PIN_LINE_7, INPUT);

  // Initialize the loop address  
  currentAddress = 0;
  
  // And initialize the output pins and the interface by stating with the first address
  writeAddressToMultiplexer(currentAddress);
  delay(100); // Wait a little for the interface
}

void loop() {
  // Read pin values. Those are binary 0 or 1 of course
  byte line_4 = digitalRead(PIN_LINE_4);
  byte line_5 = digitalRead(PIN_LINE_5);
  byte line_6 = digitalRead(PIN_LINE_6);
  byte line_7 = digitalRead(PIN_LINE_7);

  // Store in current data array
  allPins[currentAddress] = (line_4 << 4) | (line_5 << 5) | (line_6 << 6) | (line_7 << 7);

  // To avoid flickering due to unused inputs, mask them out in the resulst
  allPins[currentAddress] = allPins[currentAddress] & activeLineMask;
 
  // Setup multiplexers for next read
  byte previousAddress = currentAddress;
  currentAddress = (currentAddress + 1) & 0x0f;
  //writeAddressToMultiplexor(0x8);
  writeAddressToMultiplexer(currentAddress);

  // And only every 16th loop we do write the collected data on the serial out back the PC
  // Quick calculation - if we do 9600 Baud, we have an effective byte rate of 960 Bytes/second (start bit and stop bit)
  // With 16 Bytes per joystick readout, we get 60 Hz, which seems reasonable. Ways to accelerate - pack bits better
  // or increase serial speed. Keep it human readable for now.
  if (currentAddress == 0) {
    bool changed = false;
    for (int i = 0; i < 16; i++) {
      if (allPins[i] != previousPins[i]) {
        // At least one change
        changed = true;
      }
      previousPins[i] = allPins[i];
    }

    if (changed) {
      for (int i = 0; i < 16; i++) {
        sprintf(numberOut, "%02x ", allPins[i]);
        Serial.print(numberOut);
      }
      Serial.print("\n");
    }
  }
  
  // To get close to the 1000 Loops/second we want, delay a millisecond. not sure of this is necesary or optimal.
  //delay(1);
}

