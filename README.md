# multijoy2usb

Arduino project to connect the 1993 Multijoy Interface via USB to a contemporary computer.

Some help was found at the following locations:

https://electronics.stackexchange.com/questions/130262/connecting-a-parallel-port-to-an-arduino
https://www.arduino.cc/en/Hacking/ParallelProgrammer